// Copyright 2013 The Shellinford-Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
//
// The following codes are based on http://golang.org/src/pkg/sort/sort.go
// Copyright 2009 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package sort implemenets multikey quicksort.
package mkqsort

type CompareResult int

const (
	CompareResultLess    = CompareResult(-1)
	CompareResultEqual   = CompareResult(0)
	CompareResultGreater = CompareResult(1)
)

type Interface interface {
	// Len is len(data).
	Len() int
	// DataLen is len(data[i]).
	DataLen() int
	// CompareAt compares data[i][idx] and data[j][idx].
	CompareAt(i, j, idx int) CompareResult
	// Swap swaps the elements with indexes i and j.
	Swap(i, j int)
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func less(data Interface, i, j, idx int) bool {
	for ln := data.DataLen(); idx < ln; idx++ {
		switch data.CompareAt(i, j, idx) {
		case CompareResultLess:
			return true
		case CompareResultGreater:
			return false
		}
	}
	return false
}

// Insertion sort
func insertionSort(data Interface, a, b int, idx int) {
	for i := a + 1; i < b; i++ {
		for j := i; j > a && less(data, j, j-1, idx); j-- {
			data.Swap(j, j-1)
		}
	}
}

// siftDown implements the heap property on data[lo, hi).
// first is an offset into the array where the root of the heap lies.
func siftDown(data Interface, lo, hi, first int, idx int) {
	root := lo
	for {
		child := 2*root + 1
		if child >= hi {
			break
		}
		if child+1 < hi && less(data, first+child, first+child+1, idx) {
			child++
		}
		if !less(data, first+root, first+child, idx) {
			return
		}
		data.Swap(first+root, first+child)
		root = child
	}
}

func heapSort(data Interface, a, b int, idx int) {
	first := a
	lo := 0
	hi := b - a

	// Build heap with greatest element at top.
	for i := (hi - 1) / 2; i >= 0; i-- {
		siftDown(data, i, hi, first, idx)
	}

	// Pop elements, largest first, into end of data.
	for i := hi - 1; i >= 0; i-- {
		data.Swap(first, first+i)
		siftDown(data, lo, i, first, idx)
	}
}

// Quicksort, following Bentley and McIlroy,
// ``Engineering a Sort Function,'' SP&E November 1993.

// medianOfThree moves the median of the three values data[a], data[b], data[c] into data[a].
func medianOfThree(data Interface, a, b, c int, idx int) {
	m0 := b
	m1 := a
	m2 := c
	// bubble sort on 3 elements
	if data.CompareAt(m1, m0, idx) == CompareResultLess {
		data.Swap(m1, m0)
	}
	if data.CompareAt(m2, m1, idx) == CompareResultLess {
		data.Swap(m2, m1)
	}
	if data.CompareAt(m1, m0, idx) == CompareResultLess {
		data.Swap(m1, m0)
	}
	// now data[m0] <= data[m1] <= data[m2]
}

func swapRange(data Interface, a, b, n int) {
	for i := 0; i < n; i++ {
		data.Swap(a+i, b+i)
	}
}

func doPivot(data Interface, lo, hi int, idx int) (midlo, midhi int) {
	m := lo + (hi-lo)/2 // Written like this to avoid integer overflow.
	if hi-lo > 40 {
		// Tukey's ``Ninther,'' median of three medians of three.
		s := (hi - lo) / 8
		medianOfThree(data, lo, lo+s, lo+2*s, idx)
		medianOfThree(data, m, m-s, m+s, idx)
		medianOfThree(data, hi-1, hi-1-s, hi-1-2*s, idx)
	}
	medianOfThree(data, lo, m, hi-1, idx)

	// Invariants are:
	//      data[lo] = pivot (set up by ChoosePivot)
	//      data[lo <= i < a] = pivot
	//      data[a <= i < b] < pivot
	//      data[b <= i < c] is unexamined
	//      data[c <= i < d] > pivot
	//      data[d <= i < hi] = pivot
	//
	// Once b meets c, can swap the "= pivot" sections
	// into the middle of the slice.
	pivot := lo
	a, b, c, d := lo+1, lo+1, hi, hi
	for {
	loopFirst:
		for b < c {
			switch data.CompareAt(b, pivot, idx) {
			case CompareResultLess:
				b++
			case CompareResultEqual:
				data.Swap(a, b)
				a++
				b++
			case CompareResultGreater:
				break loopFirst
			}
		}
	loopSecond:
		for b < c {
			switch data.CompareAt(pivot, c-1, idx) {
			case CompareResultLess:
				c--
			case CompareResultEqual:
				data.Swap(c-1, d-1)
				c--
				d--
			case CompareResultGreater:
				break loopSecond
			}
		}
		if b >= c {
			break
		}
		// data[b] > pivot; data[c-1] < pivot
		data.Swap(b, c-1)
		b++
		c--
	}

	n := min(b-a, a-lo)
	swapRange(data, lo, b-n, n)

	n = min(hi-d, d-c)
	swapRange(data, c, hi-n, n)

	return lo + b - a, hi - (d - c)
}

func multiKeyQuickSort(data Interface, a, b, maxDepth int, idx int) {
	var mlo, mhi int
	ln := data.DataLen()
	for b-a > 7 {
		if maxDepth == 0 {
			heapSort(data, a, b, idx)
			return
		}
		maxDepth--
		mlo, mhi = doPivot(data, a, b, idx)
		multiKeyQuickSort(data, a, mlo, maxDepth, idx)
		multiKeyQuickSort(data, mhi, b, maxDepth, idx)
		a, b = mlo, mhi
		idx++
		if ln == idx {
			return
		}
	}
	if b-a > 1 {
		insertionSort(data, a, b, idx)
	}
}

// Sort sorts data by multiKeyQuickSort.
func Sort(data Interface) {
	// Switch to heapsort if depth of 2*ceil(lg(n+1)) is reached.
	n := data.Len()
	maxDepth := 0
	for i := n; i > 0; i >>= 1 {
		maxDepth++
	}
	maxDepth *= 2
	multiKeyQuickSort(data, 0, n, maxDepth, 0)
}
