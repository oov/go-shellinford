// Copyright 2013 The Shellinford-Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package shellinford

import (
	"bitbucket.org/oov/go-shellinford/shellinford/bit"
	"bitbucket.org/oov/go-shellinford/shellinford/bwt"
	"bitbucket.org/oov/go-shellinford/shellinford/wavelet"
	"encoding/binary"
	"fmt"
	"io"
)

type FMIndex struct {
	sv       *wavelet.Matrix
	doctails *bit.Vector
	posdic   []int
	idic     []int
	ddic     int
	head     int
	rlt      [256]int
	substr   []byte
}

func NewFMIndex() *FMIndex {
	return &FMIndex{
		doctails: bit.NewVector(),
	}
}

func OpenFMIndex(r io.Reader) (*FMIndex, error) {
	fmi := &FMIndex{}

	var err error
	b := make([]byte, 8)

	if _, err = r.Read(b); err != nil {
		return nil, err
	}
	fmi.ddic = int(binary.LittleEndian.Uint64(b))

	if _, err = r.Read(b); err != nil {
		return nil, err
	}
	fmi.head = int(binary.LittleEndian.Uint64(b))

	if fmi.sv, err = wavelet.OpenMatrix(r); err != nil {
		return nil, err
	}

	if fmi.doctails, err = bit.OpenVector(r); err != nil {
		return nil, err
	}

	fmi.buildDic()
	return fmi, nil
}

func (fmi *FMIndex) Add(doc []byte) {
	fmi.substr = append(fmi.substr, doc...)
	fmi.doctails.Set(fmi.doctails.Len()+len(doc)-1, true)
}

func (fmi *FMIndex) Len() int {
	return fmi.sv.Len()
}

func (fmi *FMIndex) DocumentLen() int {
	return fmi.doctails.RankLen(true)
}

func (fmi *FMIndex) buildDic() {
	for c := 0; c < 256; c++ {
		fmi.rlt[c] = fmi.sv.RankLessThan(fmi.sv.Len(), byte(c))
	}

	fmi.posdic = make([]int, fmi.sv.Len()/fmi.ddic+1)
	fmi.idic = make([]int, fmi.sv.Len()/fmi.ddic+1)
	i := fmi.head
	pos := fmi.Len() - 1
	for {
		if i%fmi.ddic == 0 {
			fmi.posdic[i/fmi.ddic] = pos
		}
		if pos%fmi.ddic == 0 {
			fmi.idic[pos/fmi.ddic] = i
		}
		c := fmi.sv.Get(i)
		i = fmi.rlt[c] + fmi.sv.Rank(i, c)
		pos--

		if i == fmi.head {
			break
		}
	}
}

func (fmi *FMIndex) Build(endMarker byte, ddic int) {
	fmi.doctails.Build()
	fmi.substr = append(fmi.substr, endMarker)
	s, head := bwt.Do(fmi.substr)
	fmi.head = head
	fmi.ddic = ddic
	fmi.sv = wavelet.NewMatrix(s)
	fmi.substr = fmi.substr[:0]
	fmi.buildDic()
}

func (fmi *FMIndex) Rows(key []byte) (n, first, last int) {
	i := len(key) - 1
	first = fmi.rlt[key[i]] + 1
	last = fmi.rlt[key[i]+1]
	for first <= last {
		if i == 0 {
			first--
			last--
			n = last - first + 1
			return
		}
		i--
		c := key[i]
		first = fmi.rlt[c] + fmi.sv.Rank(first-1, c) + 1
		last = fmi.rlt[c] + fmi.sv.Rank(last, c)
	}
	return
}

func (fmi *FMIndex) Pos(i int) int {
	if i >= fmi.Len() {
		panic(fmt.Sprintf("fmindex: FMIndex.Position: i:%d is out of range(len:%d)", i, fmi.Len()))
	}

	var pos int
	for i != fmi.head {
		if i%fmi.ddic == 0 {
			pos += fmi.posdic[i/fmi.ddic] + 1
			break
		}
		c := fmi.sv.Get(i)
		i = fmi.rlt[c] + fmi.sv.Rank(i, c)
		pos++
	}
	return pos % fmi.Len()
}

func (fmi *FMIndex) SubBytes(pos, ln int) []byte {
	if pos >= fmi.Len() {
		panic(fmt.Sprintf("fmindex: FMIndex.SubBytes: pos:%d is out of range(len:%d)", pos, fmi.Len()))
	}
	var posEnd int
	if pos+ln < fmi.Len() {
		posEnd = pos + ln
	} else {
		posEnd = fmi.Len()
	}
	posTmp := fmi.Len() - 1
	posIdic := (posEnd + fmi.ddic - 2) / fmi.ddic
	i := fmi.head
	if posIdic < len(fmi.idic) {
		posTmp = posIdic * fmi.ddic
		i = fmi.idic[posIdic]
	}

	fmi.substr = fmi.substr[:0]
	for posTmp >= pos {
		c := fmi.sv.Get(i)
		i = fmi.rlt[c] + fmi.sv.Rank(i, c)
		if posTmp < posEnd {
			fmi.substr = append(fmi.substr, c)
		}
		if posTmp == 0 {
			break
		}
		posTmp--
	}

	for i, j := 0, len(fmi.substr)-1; i < j; i++ {
		fmi.substr[i], fmi.substr[j] = fmi.substr[j], fmi.substr[i]
		j--
	}
	return fmi.substr
}

func (fmi *FMIndex) DocumentId(pos int) int {
	if pos >= fmi.Len() {
		panic(fmt.Sprintf("fmindex: FMIndex.DocumentId: i:%d is out of range(len:%d)", pos, fmi.Len()))
	}
	return fmi.doctails.Rank(pos, true)
}

func (fmi *FMIndex) Search(key []byte) map[int]int {
	dids := make(map[int]int)
	rows, first, last := fmi.Rows(key)
	if rows > 0 {
		for i := first; i <= last; i++ {
			did := fmi.DocumentId(fmi.Pos(i))
			dids[did]++
		}
	}
	return dids
}

func (fmi *FMIndex) Document(did int) []byte {
	if did >= fmi.DocumentLen() {
		panic(fmt.Sprintf("fmindex: FMIndex.Document: i:%d is out of range(len:%d)", did, fmi.DocumentLen()))
	}

	var pos int
	if did > 0 {
		pos = fmi.doctails.Select(did-1, true) + 1
	}
	ln := fmi.doctails.Select(did, true) - pos + 1
	return fmi.SubBytes(pos, ln)
}

func (fmi *FMIndex) Write(w io.Writer) error {
	var err error
	b := make([]byte, 8)

	binary.LittleEndian.PutUint64(b, uint64(fmi.ddic))
	if _, err = w.Write(b); err != nil {
		return err
	}

	binary.LittleEndian.PutUint64(b, uint64(fmi.head))
	if _, err = w.Write(b); err != nil {
		return err
	}

	if err = fmi.sv.Write(w); err != nil {
		return err
	}

	if err = fmi.doctails.Write(w); err != nil {
		return err
	}

	return nil
}
