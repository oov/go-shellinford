package main

// $ go run read.go

import (
	"bitbucket.org/oov/go-shellinford/shellinford"
	"fmt"
	"os"
	"time"
)

func main() {
	f, err := os.Open("index")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	st := time.Now()
	fm, err := shellinford.OpenFMIndex(f)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Ready. (%fsecs)\n", time.Now().Sub(st).Seconds())

	w := "探偵"
	st = time.Now()
	found := fm.Search([]byte(w))
	fmt.Printf("Keyword \"%s\" found %d entries. (%fsecs)\n", w, len(found), time.Now().Sub(st).Seconds())
	fmt.Println()
	for k, v := range found {
		fmt.Println(string(fm.Document(k)), v, "Hit(s)")
	}
}
